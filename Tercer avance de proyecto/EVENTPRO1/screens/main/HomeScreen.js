import React, { useEffect, useRef } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Text, SafeAreaView, Platform, Animated } from 'react-native';
import { colors as themeColors } from '../../constants/theme';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import ScreenHeader from '../../components/shared/ScreenHeader';
import TopPlacesCarousel from '../../components/Home/TopPlacesCarousel';
import SectionHeader from '../../components/shared/SectionHeader';
import TripsList from '../../components/Home/TripsList';
import { PLACES, TOP_PLACES } from '../../data';
import DrawerSceneWrapper from '../../components/menu/DrawerSceneWrapper';

const HomeScreen = ({ navigation }) => {
  const openDrawer = () => {
    navigation.openDrawer();
  };

  const animatedValue = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 1500, // Duración de la animación (ms)
      useNativeDriver: true,
    }).start();
  }, []);

  const eventProText = "EVENTPRO";

  return (
    <SafeAreaView style={styles.safeArea}>
      <DrawerSceneWrapper>
        <View style={styles.container}>
          <View style={styles.headerContainer}>
            <TouchableOpacity onPress={openDrawer} style={styles.menuButton}>
              <Icon name="menu" size={24} color="white" />
            </TouchableOpacity>
            <Animated.View style={[styles.headerTitleContainer, { opacity: animatedValue }]}>
              {eventProText.split('').map((letter, index) => (
                <Text key={index} style={styles.headerTitle}>{letter}</Text>
              ))}
            </Animated.View>
          </View>
          
          <ScreenHeader mainTitle="Find Your" secondTitle="Best option" />
          
          <ScrollView contentContainerStyle={styles.scrollViewContent} showsVerticalScrollIndicator={false}>
            <TopPlacesCarousel list={TOP_PLACES} />
            <SectionHeader
              title="Popular Municipalities"
              buttonTitle=""
              onPress={() => {}}
            />
            <TripsList list={PLACES} />
          </ScrollView>
        </View>
      </DrawerSceneWrapper>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? 0 : Platform.OS === 'ios' ? Platform.Version < 11 ? 20 : 0 : 0,
    backgroundColor: themeColors.light,
  },
  container: {
    flex: 1,
    backgroundColor: 'white', 
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 16,
    backgroundColor: 'black', // Color de fondo del contenedor
    borderRadius: 1, // Borde redondeado
    marginVertical: 1,
    elevation: 2,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowRadius: 10,
  },
  menuButton: {
    marginRight: 12,
  },
  headerTitleContainer: {
    flexDirection: 'row',
  },
  headerTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white', // Color de las letras
    marginLeft: 5, // Espacio entre las letras
  },
  scrollViewContent: {
    flexGrow: 1,
    paddingBottom: 65,
  },
});

export default HomeScreen;
