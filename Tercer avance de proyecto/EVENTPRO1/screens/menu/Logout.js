import React, { useEffect } from 'react';
import { View, StyleSheet, Animated } from 'react-native';
import LottieView from 'lottie-react-native';

const LogoutScreen = ({ navigation }) => {
  useEffect(() => {
    const timer = setTimeout(() => {
      navigation.replace('Login');
    }, 4000);

    return () => clearTimeout(timer);
  }, [navigation]);

  const confettiOpacity = new Animated.Value(0);

  const confettiIn = Animated.timing(confettiOpacity, {
    toValue: 1,
    duration: 2500,
    delay: 150,
    useNativeDriver: true,
  });

  Animated.parallel([confettiIn]).start();

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.confettiContainer, { opacity: confettiOpacity }]}>
        <LottieView
          source={require('../../assets/animations/bye.json')}
          autoPlay
          loop
          style={styles.confetti}
        />
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F6E9EA', 
  },
  confettiContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  confetti: {
    width: 390, 
    height: 390, 
  },
});

export default LogoutScreen;
