import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import * as Animatable from 'react-native-animatable';
import { Ionicons } from '@expo/vector-icons'; 
import DrawerSceneWrapper from '../../components/menu/DrawerSceneWrapper';

import API_URL from '../../apiConfig';
import Image1 from '../../assets/Transportes/basico.png';
import Image2 from '../../assets/Transportes/regular.png';
import Image3 from '../../assets/Transportes/premum.png';

const TransportScreen = ({ navigation }) => {
  const [transportes, setTransportes] = useState([]);

  useEffect(() => {
    fetchTransportes();
  }, []);

  const subHeadingRef = React.useRef(null);

  useFocusEffect(
    React.useCallback(() => {
      if (subHeadingRef.current) {
        subHeadingRef.current.bounceIn(1000);
      }
    }, [])
  );

  const fetchTransportes = async () => {
    try {
      const response = await fetch(`${API_URL}/transporte`);
      const data = await response.json();
      setTransportes(data);
    } catch (error) {
      console.error('Error fetching transports:', error);
    }
  };

  const renderPackage = (item, index) => {
    let imageSource;
    switch (index) {
      case 0:
        imageSource = Image1;
        break;
      case 1:
        imageSource = Image2;
        break;
      case 2:
        imageSource = Image3;
        break;
      default:
        imageSource = null;
    }
    
    return (
      <Animatable.View animation="fadeInUp" style={styles.packageContainer} key={index}>
        <Image source={imageSource} style={styles.packageImage} />
        <Text style={styles.packageName}>{item.nombre}</Text>
        <Text style={styles.packageName}>{item.calidad}</Text>
        <TouchableOpacity
          style={styles.descriptionButton}
          onPress={() => toggleDescription(index)}
        >
          <Text style={styles.descriptionButtonText}>Description</Text>
        </TouchableOpacity>

        {item.showDescription && (
          <Animatable.View animation="fadeIn" style={styles.descriptionContainer}>
            <Text style={styles.packageDescription}><Text style={{fontWeight: 'bold'}}>Capacity:</Text> {item.capacidad}</Text>
            <Text style={styles.packageDescription}><Text style={{fontWeight: 'bold'}}>Cost:</Text> {item.costo}</Text>
          </Animatable.View>
        )}
      </Animatable.View>
    );
  };

  const toggleDescription = (index) => {
    setTransportes(prevState =>
      prevState.map((item, i) => {
        if (i === index) {
          return { ...item, showDescription: !item.showDescription };
        }
        return item;
      })
    );
  };

  return (
    <DrawerSceneWrapper>
    <ScrollView contentContainerStyle={styles.container}>
      <TouchableOpacity onPress={() => navigation.openDrawer()} style={styles.menuButton}>
        <Ionicons name="menu" size={26} color="#009688" />
      </TouchableOpacity>
      <View style={styles.bottomSpace}></View>
      <Text style={styles.heading}>Transport List</Text>
      <Animatable.Text ref={subHeadingRef} animation="bounceIn" style={styles.subHeading}>Check out your options!</Animatable.Text>
      {transportes.map((item, index) => (
        <Animatable.View key={index} animation="fadeInUp" delay={index * 100}>
          {renderPackage(item, index)}
        </Animatable.View>
      ))}
      <View style={styles.bottomSpace}></View>
    </ScrollView>
    </DrawerSceneWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  subHeading: {
    fontSize: 24,
    marginBottom: 20,
    textAlign: 'center',
    color: '#009688',
    fontWeight: 'bold',
  },
  packageContainer: {
    marginBottom: 30,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: '#ffffff',
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  packageImage: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  packageName: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 5,
    textAlign: 'center',
    backgroundColor: 'white', // Fondo blanco para el nombre del paquete
  },
  descriptionButton: {
    backgroundColor: 'rgba(0, 150, 136, 0.7)',
    paddingVertical: 8,
    paddingHorizontal: 20,
    borderRadius: 20,
    alignSelf: 'center',
    marginTop: 10,
  },
  descriptionButtonText: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 16,
  },
  descriptionContainer: {
    paddingHorizontal: 15,
    marginTop: 10,
  },
  packageDescription: {
    fontSize: 16,
    marginBottom: 10,
    textAlign: 'center',
    backgroundColor: 'white', // Fondo blanco para la descripción del paquete
  },
  bottomSpace: {
    paddingBottom: 80,
    backgroundColor:'white' , // Espacio adicional al final del contenido
  },
  menuButton: {
    position: 'absolute',
    top: 35,
    left: 20,
    zIndex: 1, // Para que esté encima del contenido
  },
});

export default TransportScreen;
