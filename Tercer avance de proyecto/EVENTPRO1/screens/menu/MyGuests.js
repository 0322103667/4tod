import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, TextInput, FlatList, StyleSheet, Alert, Animated, KeyboardAvoidingView, Platform } from 'react-native';
import { useNavigation, DrawerActions } from '@react-navigation/native';
import DrawerSceneWrapper from '../../components/menu/DrawerSceneWrapper';
import { Ionicons } from '@expo/vector-icons';
import API_URL from '../../apiConfig';
import * as Animatable from 'react-native-animatable';

const MyGuests = () => {
  const navigation = useNavigation();
  const [hora_registro, setHoraRegistro] = useState('');
  const [nombre_empleado, setNombreEmpleado] = useState('');
  const [area, setArea] = useState('');
  const [registeredGuests, setRegisteredGuests] = useState([]);
  const CLIENTE_ID = 43; 
  const menuButtonOffset = new Animated.Value(24); 

  useEffect(() => {
    fetchRegisteredGuests();
  }, []);

  const fetchRegisteredGuests = async () => {
    try {
      const response = await fetch(`${API_URL}/lista_asistencia?clienteId=${CLIENTE_ID}`);
      const data = await response.json();
      setRegisteredGuests(data);
    } catch (error) {
      console.error('Error fetching registered guests:', error);
    }
  };

  const handleGoBackToDrawer = () => {
    navigation.dispatch(DrawerActions.openDrawer());
  };

  const handleRegisterGuest = async () => {
    try {
      const response = await fetch(`${API_URL}/lista_asistencia`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          hora_registro,
          nombre_empleado,
          area,
          cliente: CLIENTE_ID // Agregar el ID del cliente al registro
        }),
      });
      const data = await response.json();
      setHoraRegistro('');
      setNombreEmpleado('');
      setArea('');
      fetchRegisteredGuests();
    } catch (error) {
      console.error('Error registering guest:', error);
    }
  };

  const handleUpdateGuest = async (index) => {
    const { numero } = registeredGuests[index];
    try {
      const response = await fetch(`${API_URL}/lista_asistencia/${numero}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          hora_registro: registeredGuests[index].hora_registro,
          nombre_empleado: registeredGuests[index].nombre_empleado,
          area: registeredGuests[index].area
        }),
      });
      if (response.ok) {
        Alert.alert('Guest updated successfully');
        fetchRegisteredGuests();
      } else {
        Alert.alert('Error updating guest');
      }
    } catch (error) {
      console.error('Error updating guest:', error);
    }
  };

  const handleDeleteGuest = async (index) => {
    const { numero } = registeredGuests[index];
    try {
      const response = await fetch(`${API_URL}/lista_asistencia/${numero}`, {
        method: 'DELETE',
      });
      if (response.ok) {
        Alert.alert('Guest deleted successfully');
        fetchRegisteredGuests();
      } else {
        Alert.alert('Error deleting guest');
      }
    } catch (error) {
      console.error('Error deleting guest:', error);
    }
  };

  const renderItem = ({ item, index }) => (
    <Animatable.View animation="fadeInDown" delay={200 * index} style={{ marginBottom: 20 }}>
      <View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
          <TouchableOpacity onPress={() => handleUpdateGuest(index)} style={{ padding: 5, backgroundColor: 'green', borderRadius: 5, marginRight: 5 }}>
            <Text style={{ color: 'white' }}>Edit</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleDeleteGuest(index)} style={{ padding: 5, backgroundColor: 'red', borderRadius: 5 }}>
            <Text style={{ color: 'white' }}>Delete</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10, backgroundColor: 'gray' }}>
          <Text style={{ flex: 2, textAlign: 'center', fontWeight: 'bold', color: 'white' }}>Arrival Time</Text>
          <Text style={{ flex: 3, textAlign: 'center', fontWeight: 'bold', color: 'white' }}>Name</Text>
          <Text style={{ flex: 2, textAlign: 'center', fontWeight: 'bold', color: 'white' }}>Area</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10, borderBottomWidth: 1, borderBottomColor: '#c8e1ff' }}>
          <TextInput
            style={{ flex: 2, textAlign: 'center', color: 'black' }}
            value={item.hora_registro}
            onChangeText={(text) => {
              const newGuests = [...registeredGuests];
              newGuests[index].hora_registro = text;
              setRegisteredGuests(newGuests);
            }}
          />
          <TextInput
            style={{ flex: 3, textAlign: 'center', color: 'black' }}
            value={item.nombre_empleado}
            onChangeText={(text) => {
              const newGuests = [...registeredGuests];
              newGuests[index].nombre_empleado = text;
              setRegisteredGuests(newGuests);
            }}
          />
          <TextInput
            style={{ flex: 2, textAlign: 'center', color: 'black' }}
            value={item.area}
            onChangeText={(text) => {
              const newGuests = [...registeredGuests];
              newGuests[index].area = text;
              setRegisteredGuests(newGuests);
            }}
          />
        </View>
      </View>
    </Animatable.View>
  );

  const animateMenuButton = () => {
    Animated.spring(menuButtonOffset, {
      toValue: 40, // Ajustado a 40
      useNativeDriver: false,
    }).start();
  };

  useEffect(() => {
    animateMenuButton();
  }, []);

  return (
    <DrawerSceneWrapper>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
        <View style={styles.container}>
          <TouchableOpacity onPress={() => navigation.openDrawer()} style={styles.drawerButton}>
            <Ionicons name="menu" size={26} color="#009688" />
          </TouchableOpacity>
          <Animatable.View animation="slideInUp" style={styles.inputContainer}>
            <Text style={{ fontSize: 24, fontWeight: 'bold', marginBottom: 20, textAlign: 'center', alignSelf: 'center' }}>Guest List</Text>

            <TextInput
              placeholder="Arrival Time"
              value={hora_registro}
              onChangeText={setHoraRegistro}
              style={styles.input}
              placeholderTextColor="#5C6969"
            />
            <TextInput
              placeholder="Employee Name"
              value={nombre_empleado}
              onChangeText={setNombreEmpleado}
              style={styles.input}
              placeholderTextColor="#5C6969"
            />
            <TextInput
              placeholder="Area"
              value={area}
              onChangeText={setArea}
              style={styles.input}
              placeholderTextColor="#5C6969"
            />

            <TouchableOpacity onPress={handleRegisterGuest} style={styles.button}>
              <Text style={{ color: 'white', fontSize: 16, textAlign: 'center', alignSelf: 'center' }}>Register Guest</Text>
            </TouchableOpacity>
          </Animatable.View>
          <FlatList
            data={registeredGuests}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
            style={styles.flatList}
          />
        </View>
      </KeyboardAvoidingView>
    </DrawerSceneWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  drawerButton: {
    position: 'absolute',
    top: 30, // Ajustado para que esté debajo del statusBar
    left: 15, // Ajustado para que esté en la esquina izquierda
    zIndex: 1, // Ajustado para que esté por encima de otros elementos
  },
  input: {
    borderWidth: 1,
    borderColor: 'gray',
    padding: 10,
    marginVertical: 20,
    width: 200,
    backgroundColor: 'white',
  },
  button: {
    backgroundColor: '#009688',
    padding: 10,
    borderRadius: 5,
    marginTop: 20,
  },
  flatList: {
    marginTop: 20,
    maxHeight: 300,
    width: '100%',
    backgroundColor: 'white',
  },
});

export default MyGuests;
