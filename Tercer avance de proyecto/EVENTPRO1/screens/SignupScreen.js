import React, { useState, useEffect } from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView, KeyboardAvoidingView, Platform, Alert } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import Animated, { FadeIn, FadeInDown, FadeInUp } from 'react-native-reanimated';
import { Picker } from '@react-native-picker/picker';
import { Ionicons } from '@expo/vector-icons'; 
import API_URL from '../apiConfig'; 

const API_URL_CLIENTE = `${API_URL}/cliente`;
const API_URL_MUNICIPIO = `${API_URL}/municipio`;

export default function SignUpScreen({ navigation }) {
  const [nombre, setNombre] = useState('');
  const [correo, setCorreo] = useState('');
  const [password, setPassword] = useState('');
  const [num_tel, setNumTel] = useState('');
  const [rfc, setRfc] = useState('');
  const [municipioSeleccionado, setMunicipioSeleccionado] = useState('');
  const [campoMunicipioVisible, setCampoMunicipioVisible] = useState(false);
  const [municipios, setMunicipios] = useState([]);
  const [direccionVisible, setDireccionVisible] = useState(false);
  const [dir_calle, setDirCalle] = useState('');
  const [dir_num, setDirNum] = useState('');
  const [dir_colonia, setDirColonia] = useState('');
  const [municipioCodigo, setMunicipioCodigo] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    fetchMunicipios();
  }, []);

  const fetchMunicipios = async () => {
    try {
      const response = await fetch(API_URL_MUNICIPIO);
      const data = await response.json();
      setMunicipios(data);
    } catch (error) {
      console.error('Error al obtener los municipios:', error);
    }
  };

  const handleSignUp = async () => {
    try {
      if (!validarCorreo(correo)) {
        Alert.alert('Error', 'Invalid email. Please enter a valid email address.');
        return;
      }
      if (!validarTelefono(num_tel)) {
        Alert.alert('Error', 'Invalid phone number. Please enter a valid 10-digit phone number.');
        return;
      }
      if (!validarDireccion(dir_num)) {
        Alert.alert('Error', 'Invalid address number. Please enter only numbers in the address number field.');
        return;
      }
      if (!validarRFC(rfc)) {
        Alert.alert('Error', 'Invalid RFC. Please enter a valid RFC.');
        return;
      }

      const response = await fetch(API_URL_CLIENTE, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          nombre,
          correo,
          password,
          num_tel,
          rfc,
          dir_calle,
          dir_num,
          dir_colonia,
          municipio: municipioCodigo,
        }),
      });
      if (response.ok) {
        console.log('La cuenta se ha creado exitosamente.');
        navigation.navigate('Login');
        alert('You have been successfully registered.');
      } else {
        console.error('Error al crear cliente:', response.status);
      }
    } catch (error) {
      console.error('Error al crear cliente:', error);
    }
  };

  const validarCorreo = (correo) => {
    const regexCorreo = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regexCorreo.test(correo);
  };

  const validarTelefono = (telefono) => {
    const regexTelefono = /^\d{10}$/;
    return regexTelefono.test(telefono);
  };

  const validarDireccion = (dirNum) => {
    const regexDirNum = /^[0-9]*$/;
    return regexDirNum.test(dirNum);
  };

  const validarRFC = (rfc) => {
    const regexRFC = /^[A-Z]{4}\d{6}[A-Z0-9]{3}$/;
    return regexRFC.test(rfc);
  };

  const handleCampoMunicipioPress = () => {
    setCampoMunicipioVisible(!campoMunicipioVisible);
  };

  const handleMunicipioSeleccionado = (municipio) => {
    const municipioData = municipios.find(item => item.nombre === municipio);
    setMunicipioSeleccionado(municipio);
    setMunicipioCodigo(municipioData.codigo);
    setCampoMunicipioVisible(false);
  };

  const toggleDireccionVisible = () => {
    setDireccionVisible(!direccionVisible);
  };

  return (
    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: 'white' }} behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <StatusBar style="light" />
      <Image style={{ flex: 1, position: 'absolute', width: '100%', height: '100%' }} source={require('../assets/images/home7.jpeg')} resizeMode="cover" />

      <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: 15, marginTop: 85}}>
        <View style={{ alignItems: 'center', marginBottom: 20 }}>
          <Animated.View entering={FadeInUp.duration(1000).springify()}>
            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 51 }}>SignUp</Text>
          </Animated.View>
        </View>
        <ScrollView style={{ marginBottom: 50 }}>
          <Animated.View entering={FadeInDown.delay(100).duration(1000).springify()} style={{ marginBottom: 5 }}>
            <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10 }}>
              <TextInput placeholder="Name" placeholderTextColor={'white'} style={{ color: 'white' }} onChangeText={setNombre} />
            </View>
          </Animated.View>

          <Animated.View entering={FadeInDown.delay(200).duration(1000).springify()} style={{ marginBottom: 5 }}>
            <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10 }}>
              <TextInput placeholder="Email" placeholderTextColor={'white'} style={{ color: 'white' }} onChangeText={setCorreo} />
            </View>
          </Animated.View>

          <Animated.View entering={FadeInDown.delay(300).duration(1000).springify()} style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10 }}>
            <TextInput placeholder="Password" placeholderTextColor={'white'} secureTextEntry={!showPassword} style={{ color: 'white', paddingRight: 30 }} onChangeText={setPassword} />
            <TouchableOpacity style={{ position: 'absolute', right: 10, top: 10 }} onPress={() => setShowPassword(!showPassword)}>
              <Ionicons name={showPassword ? 'eye-off' : 'eye'} size={22} color="white" />
            </TouchableOpacity>
          </Animated.View>

          <Animated.View entering={FadeInDown.delay(400).duration(1000).springify()} style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10 }}>
            <TextInput placeholder="phone" placeholderTextColor={'white'} style={{ color: 'white' }} onChangeText={setNumTel} />
          </Animated.View>

          <TouchableOpacity onPress={toggleDireccionVisible}>
            <Animated.View entering={FadeInDown.delay(500).duration(1000).springify()} style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10 }}>
              <Text style={{ color: 'white' }}>Address</Text>
            </Animated.View>
          </TouchableOpacity>

          {direccionVisible && (
            <View>
              <Animated.View entering={FadeInDown.delay(600).duration(1000).springify()} style={{ flexDirection: 'row', marginBottom: 10 }}>
                <View style={{ flex: 1, marginRight: 5 }}>
                  <Animated.View entering={FadeInDown.delay(600).duration(1000).springify()} style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10 }}>
                    <TextInput placeholder="Street" placeholderTextColor={'white'} style={{ color: 'white' }} onChangeText={setDirCalle} />
                  </Animated.View>
                </View>
                <View style={{ flex: 1, marginLeft: 5 }}>
                  <Animated.View entering={FadeInDown.delay(600).duration(1000).springify()} style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10 }}>
                    <TextInput placeholder="Number" placeholderTextColor={'white'} style={{ color: 'white' }} onChangeText={setDirNum} />
                  </Animated.View>
                </View>
              </Animated.View>

              <Animated.View entering={FadeInDown.delay(700).duration(1000).springify()} style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10 }}>
                <TextInput placeholder="Colony" placeholderTextColor={'white'} style={{ color: 'white' }} onChangeText={setDirColonia} />
              </Animated.View>
            </View>
          )}

          <Animated.View entering={FadeInDown.delay(800).duration(1000).springify()} style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10 }}>
            <TextInput placeholder="RFC" placeholderTextColor={'white'} style={{ color: 'white' }} onChangeText={setRfc} />
          </Animated.View>

          <TouchableOpacity onPress={handleCampoMunicipioPress}>
            <Animated.View entering={FadeInDown.delay(900).duration(1000).springify()} style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10 }}>
              <Text style={{ color: 'white' }}>{municipioSeleccionado ? municipioSeleccionado : 'Select Municipality'}</Text>
            </Animated.View>
          </TouchableOpacity>

          {campoMunicipioVisible && (
            <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', padding: 15, borderRadius: 20, marginBottom: 10, paddingBottom: 10 }}>
              <View style={{ backgroundColor: 'white', borderRadius: 15, borderWidth: 1, borderColor: 'rgba(0,0,0,0.5)', maxHeight: 180 }}>
                <Picker
                  selectedValue={municipioSeleccionado}
                  onValueChange={(itemValue, itemIndex) => handleMunicipioSeleccionado(itemValue)}
                >
                  {municipios.map((municipio) => (
                    <Picker.Item key={municipio.codigo} label={municipio.nombre} value={municipio.nombre} />
                  ))}
                </Picker>
              </View>
            </View>
          )}

          <Animated.View entering={FadeInDown.delay(1000).duration(1000).springify()} style={{ marginBottom: 5 }}>
            <TouchableOpacity onPress={handleSignUp} style={{ backgroundColor: '#4ECDC4', padding: 15, borderRadius: 20, marginBottom: 5 }}>
              <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>Sign up</Text>
            </TouchableOpacity>
          </Animated.View>

          <Animated.View entering={FadeInDown.delay(1100).duration(1000).springify()} style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 20 }}>
            <Text style={{ color: 'white' }}>Already have an account? </Text>
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <Text style={{ color: 'black' }}>Login</Text>
            </TouchableOpacity>
          </Animated.View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
}
