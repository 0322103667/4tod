import React, { useRef } from 'react';
import { StatusBar, Platform } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/Main/HomeScreen';
import SearchScreen from '../screens/Main/SearchScreen';
import PackagesScreen from '../screens/Main/PackagesScreen';
import UserScreen from '../screens/Main/UserScreen'; 
import CreateScreen from '../screens/Main/CreateScreen';
import TripDetailsScreen from '../screens/Main/TripDetailsScreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import TabButton from '../components/TabButton/TabButton';
import { useNavigation } from '@react-navigation/native';
import MyEvent from '../screens/menu/MyEvent';
import MyGuests from '../screens/menu/MyGuests';
import Transportation from '../screens/menu/Transportation';
import CommentsScreen from '../screens/menu/CommentsScreen';
import Salons from '../screens/menu/Salons';
import Payments from '../screens/menu/Payments';
import LoginScreen from '../screens/LoginScreen';
import Logout from '../screens/menu/Logout';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  const navigation = useNavigation();
  const titleRef = useRef(null);

 return (
  
    <Tab.Navigator
      initialRouteName="Main"
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarIconStyle: {
          width: 40,
          height: 40,
          justifyContent: 'center',
          alignItems: 'center',
        },
        tabBarInactiveTintColor: Colors.gray, // Cambiado a Colors.gray
        tabBarActiveTintColor: Colors.primary, // Cambiado a Colors.primary
        tabBarStyle: styles.tabBar,
      }}>
      <Tab.Screen
        name="Main"
        component={DrawerNavigator}
        options={{
          tabBarIcon: ({ focused }) => (
            <TabButton
              item={{ icon: 'home-outline', title: 'Home' }}
              accessibilityState={{ selected: focused }}
              onPress={() => navigation.navigate('Main')}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Search"
        component={SearchScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <TabButton
              item={{ icon: 'magnify', title: 'Search' }}
              accessibilityState={{ selected: focused }}
              onPress={() => navigation.navigate('Search')}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Create"
        component={CreateScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <TabButton
              item={{ icon: 'plus', title: 'Create' }}
              accessibilityState={{ selected: focused }}
              onPress={() => navigation.navigate('Create')}
            />
          ),
        }}
      />
     <Tab.Screen
          name="Packages"
          component={PackagesScreen}
          options={{
            tabBarIcon: ({ focused }) => (
              <TabButton
                item={{ icon: 'shopping-outline', title: 'Packages' }}
                accessibilityState={{ selected: focused }}
                onPress={() => navigation.navigate('Packages')}
              />
            ),
          }}
        />
      <Tab.Screen
        name="User"
        component={UserScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <TabButton
              item={{ icon: 'account-outline', title: 'User' }}
              accessibilityState={{ selected: focused }}
              onPress={() => navigation.navigate('User')}
            />
          ),
        }}
      />
    </Tab.Navigator>
 );
};
const DrawerNavigator = () => {
  const drawerIcon = ({ focused, size }, name) => {
    return (
      <Icon
        name={name}
        size={size}
        color={focused ? '#FFFFFF' : '#FFFFFF'}
      />
    );
  };

  return (
    <Drawer.Navigator
      drawerType="slide"
      screenOptions={{
        headerShown: false,
        drawerActiveBackgroundColor: Colors.transparent,
        drawerInactiveBackgroundColor: Colors.transparent,
        drawerActiveTintColor: '#FFFFFF',
        drawerInactiveTintColor: '#FFFFFF',
        drawerHideStatusBarOnOpen: Platform.OS === 'ios' ? true : false,
        overlayColor: Colors.transparent,
        drawerStyle: {
          backgroundColor: Colors.bg,
          width: '60%',
        },
        sceneContainerStyle: {
          backgroundColor: Colors.bg,
        },
      }}>
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        options={{
          drawerIcon: options => drawerIcon(options, 'home-outline'),
        }}
      />
      <Drawer.Screen
        name="Transportation"
        component={Transportation}
        options={{
          drawerIcon: options => drawerIcon(options, 'bus'), 
        }}
      />
      <Drawer.Screen
        name="Salons"
        component={Salons}
        options={{
          drawerIcon: options => drawerIcon(options, 'store'), 
        }}
      />
       <Drawer.Screen
        name="My Guests"
        component={MyGuests}
        options={{
          drawerIcon: options => drawerIcon(options, 'account-group'), 
        }}
      />
      <Drawer.Screen
        name="My Events"
        component={MyEvent}
        options={{
          drawerIcon: options => drawerIcon(options, 'calendar-outline'),
        }}
      />
      <Drawer.Screen
        name="Comments"
        component={CommentsScreen}
        options={{
          drawerIcon: options => drawerIcon(options, 'comment'), 
        }}
      />
      <Drawer.Screen
        name="Payments"
        component={Payments}
        options={{
          drawerIcon: options => drawerIcon(options, 'credit-card-outline'), 
        }}

      />
  <Drawer.Screen
  name="Logout"
  component={Logout}
  options={{
    drawerIcon: ({ focused, color, size }) => (
      <Icon name={focused ? "logout-variant" : "exit-to-app"} size={size} color={color} />
    ),
  }}
/>


    </Drawer.Navigator>
  );
};


const MainNavigator = () => {
 return (
    <>
      <StatusBar hidden />
      <Stack.Navigator>
        <Stack.Screen
          name="Tab"
          component={TabNavigator}
          options={{
            headerShown: false,
            useNativeDriver: true,
            cardStyleInterpolator: ({ current: { progress } }) => ({
              cardStyle: {
                opacity: progress,
              },
            }),
          }}
        />
        <Stack.Screen
          name="TripDetails"
          component={TripDetailsScreen}
          options={{
            headerShown: false,
            useNativeDriver: true,
            cardStyleInterpolator: ({ current: { progress } }) => ({
              cardStyle: {
                opacity: progress,
              },
            }),
          }}
        />
      </Stack.Navigator>
    </>
 );
};

// Declaración de estilos
const styles = {
  tabBar: {
       height: 82, // Reduced height to make the tab bar thinner
       position: 'absolute',
       bottom: 25,
       marginHorizontal: 14,
       borderRadius: 16, // Rounded border
       justifyContent: 'center',
       alignItems: 'center',
       borderWidth: 0.1,
       borderColor: '#009688'
  }
 };
 

const Colors = {
  bg: '#009688',
  active: 'white',
  inactive: '#eee',
  transparent: 'transparent',
};
export default MainNavigator;
