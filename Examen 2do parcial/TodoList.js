// TodoList.js
import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';

const TodoList = ({ todos }) => {
  return (
    <FlatList
      data={todos}
      keyExtractor={({ id }) => id.toString()}
      renderItem={({ item }) => (
        <View style={styles.listItem}>
          <Text style={styles.listItemText}>
            ID: {item.id}
            {item.title && `: ${item.title}`}
          </Text>
          {item.userId && <Text style={styles.userIdText}>UserID: {item.userId}</Text>}
        </View>
      )}
    />
  );
};

const styles = StyleSheet.create({
  listItem: {
    padding: 15,
    marginVertical: 8,
    backgroundColor: '#ECEFF1', // Color de fondo del ítem de la lista
    borderColor: '#90A4AE', // Color del borde del ítem de la lista
    borderWidth: 4,
    borderRadius: 8,
  },
  listItemText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#263238', // Color del texto del ítem de la lista
  },
  userIdText: {
    fontSize: 16,
    color: '#263238', // Color del texto del UserID
    marginTop: 5,
  },
});

export default TodoList;
