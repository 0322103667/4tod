import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';

const FilterButton = ({ text, onPress }) => (
  <TouchableOpacity onPress={onPress} style={styles.button}>
    <Text style={styles.buttonText}>{text}</Text>
  </TouchableOpacity>
);

const App = () => {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [filterType, setFilterType] = useState('');

  useEffect(() => {
    fetchData('http://jsonplaceholder.typicode.com/todos');
  }, []);

  const fetchData = async (url) => {
    try {
      const response = await fetch(url);
      const data = await response.json();
      setData(data);
    } catch (error) {
      console.error('Error fetching data: ', error);
    }
  };

  const applyFilter = (type) => {
    setFilterType(type);
    let filtered = [];
    switch (type) {
      case 'allIds':
        filtered = data.map(item => ({ id: item.id }));
        break;
      case 'IdsAndTitles':
        filtered = data.map(item => ({ id: item.id, title: item.title }));
        break;
      case 'uncompletedTitle':
        for (const item of data) {
          if (!item.completed) {
            filtered.push({ id: item.id, title: item.title });
          }
        }
        break;
      case 'completedTitle':
        for (const item of data) {
          if (item.completed) {
            filtered.push({ id: item.id, title: item.title });
          }
        }
        break;
      case 'idsAndUserId':
        filtered = data.map(item => ({ id: item.id, userId: item.userId }));
        break;
      case 'completedAndUserTitle':
        for (const item of data) {
          if (item.completed) {
            filtered.push({ title: item.title, userId: item.userId });
          }
        }
        break;
      case 'uncompletedAndUserTitle':
        for (const item of data) {
          if (!item.completed) {
            filtered.push({ title: item.title, userId: item.userId });
          }
        }
        break;
      default:
        filtered = [];
        break;
    }
    setFilteredData(filtered);
  };

  const clearFilter = () => {
    setFilterType('');
    setFilteredData([]);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Lista de Pendientes</Text>
      <View style={styles.buttonsContainer}>
        <FilterButton text="Todos los IDs" onPress={() => applyFilter('allIds')} />
        <FilterButton text="IDs y Títulos" onPress={() => applyFilter('IdsAndTitles')} />
        <FilterButton text="Sin Completar" onPress={() => applyFilter('uncompletedTitle')} />
        <FilterButton text="Completados" onPress={() => applyFilter('completedTitle')} />
        <FilterButton text="IDs y UserID" onPress={() => applyFilter('idsAndUserId')} />
        <FilterButton text="Completados y UserID" onPress={() => applyFilter('completedAndUserTitle')} />
        <FilterButton text="Sin Completar y UserID" onPress={() => applyFilter('uncompletedAndUserTitle')} />
        <TouchableOpacity onPress={clearFilter} style={styles.clearButton}>
          <Text style={styles.clearButtonText}>Limpiar</Text>
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.scrollView}>
        {filteredData.map((item, index) => (
          <View key={index} style={styles.dataItem}>
            {filterType.includes('Id') && <Text style={styles.text}>{`ID: ${item.id}`}</Text>}
            {filterType.includes('Title') && <Text style={styles.text}>{`Título: ${item.title}`}</Text>}
            {filterType.includes('User') && <Text style={styles.text}>{`UserID: ${item.userId}`}</Text>}
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#f0f0f0',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: '#333',
  },
  buttonsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#007bff',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
    marginRight: 10,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
  clearButton: {
    backgroundColor: '#dc3545',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  clearButtonText: {
    color: '#fff',
    textAlign: 'center',
  },
  scrollView: {
    flex: 1,
  },
  dataItem: {
    marginBottom: 10,
    padding: 10,
    backgroundColor: '#f4f4f4',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },
});

export default App;
