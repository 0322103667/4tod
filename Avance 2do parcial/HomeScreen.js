import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { colors as themeColors } from '../../constants/theme';
import MainHeader from '../../components/MainHeader';
import ScreenHeader from '../../components/ScreenHeader';
import TopPlacesCarousel from '../../components/TopPlacesCarousel';
import SectionHeader from '../../components/SectionHeader';
import TripsList from '../../components/TripsList';
import { PLACES, TOP_PLACES } from '../../data';

const HomeScreen = () => {
  return (
    <View style={styles.container}>
      <MainHeader title=" EVENTPRO" />
      <ScreenHeader mainTitle="Find Your" secondTitle="Dream option" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <TopPlacesCarousel list={TOP_PLACES} />
        <SectionHeader
          title="Popular hotels"
          buttonTitle="See All"
          onPress={() => {}}
        />
        <TripsList list={PLACES} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themeColors.light,
  },
});

export default HomeScreen;
