import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
// import CarInfo from './components/CarInfo';
// import CarList from './components/CarList';
// import CarDetails from './components/CarDetails';
import Post from './components/Post';
import Products from './components/Products';

export default function App() {
  return (
    <View style={styles.container}>
      {/* <CarInfo />
      <CarList />
      <CarDetails /> */}
      {/* <Post/> */}
      <Products/>
      
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
