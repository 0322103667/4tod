import React from "react";
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';

// Importa tus pantallas
import Homescreen from "./screens/Homescreen";
import Profiler from './screens/Profiler';
import SettingsScreen from "./screens/SettingsScreen";

// Importa los íconos que deseas utilizar
import { Ionicons } from '@expo/vector-icons';

const createBottomTab = () => {
  // Define tu navegador de pestañas inferior
  const TabNavigator = createBottomTabNavigator({
    Home: {
      screen: Homescreen,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) => (
          <Ionicons
            name={focused ? 'ios-home' : 'ios-home-outline'}
            size={25}
            color={tintColor}
          />
        ),
      },
    },
    Profiler: {
      screen: Profiler,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) => (
          <Ionicons
            name={focused ? 'ios-person' : 'ios-person-outline'}
            size={25}
            color={tintColor}
          />
        ),
      },
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) => (
          <Ionicons
            name={focused ? 'ios-settings' : 'ios-settings-outline'}
            size={25}
            color={tintColor}
          />
        ),
      },
    },
    // Agrega más pestañas aquí si es necesario
  }, {
    tabBarOptions: {
      activeTintColor: 'blue', // Color de la pestaña activa
      inactiveTintColor: 'gray', // Color de la pestaña inactiva
    },
  });}
  
