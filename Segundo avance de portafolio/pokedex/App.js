User
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList, ActivityIndicator } from 'react-native';

const url = "https://pokeapi.co/api/v2/pokemon?limit=150"; //obtener los primeros 150 Pokémon

export default function Pokedex() {
  const [pokemonData, setPokemonData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(data => {
        setPokemonData(data.results);
        setIsLoading(false);
      })
      .catch(error => console.error('Error fetching data:', error));
  }, []);

  if (isLoading) {
    return (
      <View style={styles.container}>
        <Text>Loading Data ...</Text>
        <ActivityIndicator size='large' color="green" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={pokemonData}
        renderItem={({ item }) => (
          <View style={styles.itemContainer}>
            <Text>{item.name}</Text>
            <Image
              source={{ uri: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${getIdFromUrl(item.url)}.png` }}
              style={styles.image}
            />
          </View>
        )}
        keyExtractor={item => item.name}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  },
  image: {
    width: 50,
    height: 50,
    marginLeft: 10
  }
});

function getIdFromUrl(url) {
  const id = url.split('/').filter(part => part.trim() !== '').pop();
  return id;
}
