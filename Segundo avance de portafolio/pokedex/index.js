import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList, ActivityIndicator } from 'react-native';

const url = "https://pokeapi.demo.dev/pokemon";

export default function Pokedex() {
  const [pokemonData, setPokemonData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(data => {
        setPokemonData(data);
        setIsLoading(false);
      })
      .catch(error => console.error('Error fetching data:', error));
  }, []);

  if (isLoading) {
    return (
      <View style={styles.container}>
        <Text>Loading Data ...</Text>
        <ActivityIndicator size='large' color="green" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={pokemonData}
        renderItem={({ item }) => (
          <View style={styles.itemContainer}>
            <Text>{item.name}</Text>
            <Image source={{ uri: item.image }} style={styles.image} />
          </View>
        )}
        keyExtractor={item => item.id.toString()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  },
  image: {
    width: 50,
    height: 50,
    marginLeft: 10
  }
});
