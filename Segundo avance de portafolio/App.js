import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';

const url = "https://api.coindesk.com/v1/bpi/currentprice.json";

export default function App() {
 const [response, setResponse] = useState(null);
 const [isLoading, setIsLoading] = useState(true);
 const [error, setError] = useState(null);

 useEffect(() => {
    fetch(url)
      .then((res) => {
        if (!res.ok) {
          throw new Error('Failed to load data');
        }
        return res.json();
      })
      .then((result) => {
        setResponse(result);
        setIsLoading(false);
      })
      .catch((error) => {
        setError(error.toString());
        setIsLoading(false);
      });
 }, []);

 const getContent = () => {
    if (isLoading) {
      return (
        <View>
          <Text style={styles.textSize}>Loading Data...</Text>
          <ActivityIndicator size="large"/>
        </View>
      );
    } else if (error) {
      return (
        <Text>{error}</Text>
      );
    } else {
      return (
        <View>
          <Text style={styles.textSize}>BTC to USD: $ {response.bpi.USD.rate}</Text>
          <Text style={styles.textSize}>BTC to EUR: € {response.bpi.EUR.rate}</Text>
          <Text style={styles.textSize}>Trading Update: {response.time.updated}</Text>
        </View>
      );
    }
 }

 return (
    <View style={styles.container}>
      {getContent()}
    </View>
 );
}

const styles = StyleSheet.create({
 container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
 },
 textSize: {
    fontSize: 16,
 },
});